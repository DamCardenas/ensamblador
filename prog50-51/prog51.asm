;programa prog51.asm --- 
;Programa que dibuja una banderita y al presionar cualquier tecla 
;se desplaza hacia la derecha...

Pintar MACRO color, eIni, eFin
		mov ah, 06h			;Función para "limpiar" la pantalla
		mov al, 00h			;Líneas a desplazar
		mov bh, color		;Color de la fuente y el fondo
		mov cx, eIni		;Esq Sup Izq del área a "limpiar"
		mov dx, eFin		;Esq Inf Der del área a "limpiar"
		int 10h
ENDM

Banderita MACRO	inicio
		mov cx, inicio
		mov dh, ch
		mov dl, cl
		Pintar 27h, cx, dx
ENDM

Imprimir MACRO Mensaje
	mov ah, 09h
	mov dx, offset Mensaje
	int 21h
ENDM

ReadKey MACRO
	mov ah, 08h
	int 21h
ENDM
		
Terminar MACRO 
	mov ax, 4c00h
	int 21h
ENDM


.MODEL SMALL
.STACK 20h
.DATA
	cDescrip DB 'Programa que dibuja una banderita y luego la desplaza hacia la derecha',10,13,'$'
	cFinProg DB 10,13,'Fin del programa$'
	vCol1	 DW 00h
	vCol2	 DW 00h
	vVariable DW 0909h
	
.CODE
	inicio: 
		mov ax, @Data
		mov ds, ax	
		
		Pintar 07h, 0000h, 184fh	;limpiar pantalla
		Banderita 0909h
		
		mov ah, 07h ; leer caracter sin eco
		int 21h
		
		eDesplaza:
			Pintar 07h, 0000h, 184fh	;limpiar pantalla
			Banderita vVariable
			add vVariable, 02h
		
		
		
			ReadKey
			cmp dl, 4fh
			jb  eDesplaza
		
		
		
		ReadKey
		Terminar
				
		END inicio