;Programa en ensamblador, el cual muestra dos cadenas
;de caracteres en pantalla...
.MODEL SMALL
.STACK 20h
.DATA
	cSaludo 	DB 'Hola chavos!',10,13,'$'
	cDespedida 	DB 10,13,'Adios$'
.CODE
	inicio: 
		mov ax, @Data
		mov ds, ax
		
		;mov ds, @DATA
		
		mov dx, offset cSaludo
		mov ah, 09h
		int 21h

		mov dx, offset cDespedida
		mov ah, 09h
		int 21h
		
		
		mov ah, 08h				;Simula un ReadKey()
		int 21h		

		mov ax, 4c00h
		int 21h
	END inicio