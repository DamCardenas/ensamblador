;Programa que lee un caracter desde teclado y al final indica
;si el mismo es numero, letra o ninguno de ellos...

.MODEL SMALL
.STACK 20h
.DATA
	cDescrip 	DB '** Programa que lee un caracter e indica si es o no es un numero**',10,13,'$'
	cDespedida 	DB 10,13,'Adios$'
	cIngresar	DB 10,13,'Ingrese un caracter: $'
	cEsNum	    DB 10,13,'El caracter ingresado SI ES un numero $'
	cEsLetra    DB 10,13,'El caracter ingresado SI ES una letra $'
	cEsEspecial DB 10,13,'El caracter ingresado ES ESPECIAL $'
	
.CODE
	inicio: 
		mov ax, @Data
		mov ds, ax
		
		mov dx, offset cDescrip
		mov ah, 09h
		int 21h

		mov dx, offset cIngresar
		int 21h

		mov ah, 01
		int 21h
		
		cmp al, '0'			
		jb	eEsEspecial
		cmp al, '9'
		jbe eEsNumero
		cmp al, 'A'
		jb  eEsEspecial
		cmp al, 'z'
		jbe eEsLetra
		jmp eEsEspecial
		
		eEsNumero:
			mov ah, 09h
			mov dx, offset cEsNum
			int 21h
			jmp eFinProg
			
		eEsEspecial:
			mov ah, 09h
			mov dx, offset cEsEspecial
			int 21h
			jmp eFinProg
			
		eEsLetra:
			mov ah, 09h
			mov dx, offset cEsLetra
			int 21h

		
		eFinProg:
			mov ah, 09h
			mov dx, offset cDespedida
			int 21h
		
			mov ah, 08h				;Simula un ReadKey()
			int 21h		

			mov ax, 4c00h
			int 21h
	END inicio