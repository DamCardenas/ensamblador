;Programa en ensamblador, el cual lee un caracter desde teclado
;y posteriormente muestra el caracter ingresado...

.MODEL SMALL
.STACK 20h
.DATA
	cDescrip 	DB '** Programa que lee un caracter **',10,13,'$'
	cDespedida 	DB 10,13,'Adios$'
	cIngresar	DB 10,13,'Ingrese un caracter: $'
	cIngresado	DB 10,13,'El caracter ingresado fue '
.CODE
	inicio: 
		mov ax, @Data
		mov ds, ax
		
		mov dx, offset cDescrip
		mov ah, 09h
		int 21h

		mov dx, offset cIngresar
		int 21h

		mov ah, 01
		int 21h
		
		mov bl, al

		mov ah, 09h
		mov dx, offset cIngresado
		int 21h

		mov ah, 02h
		mov dl, bl
		int 21h
		
		mov ah, 09h
		mov dx, offset cDespedida
		int 21h
		
		
		mov ah, 08h				;Simula un ReadKey()
		int 21h		

		mov ax, 4c00h
		int 21h
	END inicio