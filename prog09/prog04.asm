;Programa en ensamblador, el cual lee un caracter desde teclado...
.MODEL SMALL
.STACK 20h
.DATA
	cDescrip 	DB '** Programa que lee un caracter **',10,13,'$'
	cDespedida 	DB 10,13,'Adios$'
	cIngresar	DB 'Ingrese un caracter: $'
	
.CODE
	inicio: 
		mov ax, @Data
		mov ds, ax
		
		mov dx, offset cDescrip
		mov ah, 09h
		int 21h

		mov dx, offset cIngresar
		int 21h

		mov ah, 01
		int 21h
		
		mov ah, 09h
		mov dx, offset cDespedida
		int 21h
		
		
		mov ah, 08h				;Simula un ReadKey()
		int 21h		

		mov ax, 4c00h
		int 21h
	END inicio