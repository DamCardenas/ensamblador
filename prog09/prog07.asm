;Programa que lee un caracter desde teclado y al final indica
;si el mismo es una letra 'a', en caso de que no sea, no muestra
;nada (condicional doble)

.MODEL SMALL
.STACK 20h
.DATA
	cDescrip 	DB '** Programa que lee un caracter simulando un condicional simple**',10,13,'$'
	cDespedida 	DB 10,13,'Adios$'
	cIngresar	DB 10,13,'Ingrese un caracter: $'
	cSiEsA		DB 10,13,'El caracter ingresado es una a $'
	cNoEsA		DB 10,13,'El caracter ingresado NO ES una a $'
	
.CODE
	inicio: 
		mov ax, @Data
		mov ds, ax
		
		mov dx, offset cDescrip
		mov ah, 09h
		int 21h

		mov dx, offset cIngresar
		int 21h

		mov ah, 01
		int 21h
		
		cmp al, 61h				;compara si el caracter ingresado	
		je	eSiEsA				;es una letra 'a'
		jmp eNoEsA
		
		eSiEsA:
			mov ah, 09h
			mov dx, offset cSiEsA
			int 21h
			jmp eFinProg
		
		eNoEsA:
			mov ah, 09h
			mov dx, offset cNoEsA
			int 21h
		
		eFinProg:
			mov ah, 09h
			mov dx, offset cDespedida
			int 21h
		
			mov ah, 08h				;Simula un ReadKey()
			int 21h		

			mov ax, 4c00h
			int 21h
	END inicio