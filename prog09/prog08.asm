;Programa que lee un caracter desde teclado y al final indica
;si el mismo es o no es un número...

.MODEL SMALL
.STACK 20h
.DATA
	cDescrip 	DB '** Programa que lee un caracter e indica si es o no es un numero**',10,13,'$'
	cDespedida 	DB 10,13,'Adios$'
	cIngresar	DB 10,13,'Ingrese un caracter: $'
	cSiEsNum	DB 10,13,'El caracter ingresado SI ES un numero $'
	cNoEsNum	DB 10,13,'El caracter ingresado NO ES un numero $'
	
.CODE
	inicio: 
		mov ax, @Data
		mov ds, ax
		
		mov dx, offset cDescrip
		mov ah, 09h
		int 21h

		mov dx, offset cIngresar
		int 21h

		mov ah, 01
		int 21h
		
		cmp al, 30h			
		jb	eNoEsNum
		cmp al, 39h
		jbe eSiEsNum
		jmp eNoEsNum
		
		eSiEsNum:
			mov ah, 09h
			mov dx, offset cSiEsNum
			int 21h
			jmp eFinProg
			
		eNoEsNum:
			mov ah, 09h
			mov dx, offset cNoEsNum
			int 21h
		
		eFinProg:
			mov ah, 09h
			mov dx, offset cDespedida
			int 21h
		
			mov ah, 08h				;Simula un ReadKey()
			int 21h		

			mov ax, 4c00h
			int 21h
	END inicio