; Programa en ensamblador con sintaxis MASM

.model SMALL ; Segmento donde se selecciona el modelo de tipo pequeño o tiny 
.stack 20h
.data  ; Segmento de declaracion de variables
    saludo db 10,13,'Hola BitBucket.org$' ; Variable saludo
.code ; Segmento de codigo ejecutable
    inicio: ; Label o etiqueta de inicio
        ; Precarga de segmento de datos
        mov ax,@data
        mov ds,ax

        ; Despliegue de variables en pantalla
        mov dx,offset saludo
        mov ah,09h
        int 21h

        ; Simulacion de readkey()
        mov ah,08h
        int 21h
        
        ; Terminacion del programa
        mov ax,4c00h
        int 21h
    end inicio ; Fin de etiqueta de inicio