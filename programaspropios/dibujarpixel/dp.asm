.model SMALL
.stack 20h
.data
    con db 30
    posx db 50
    posy db 100
.code
    main:
        ; Cambio a modo video
        mov al,13h  ; the mode
        mov ah,0   ; function number 0
        int 10h    ; call BIOS Screen Service


        ; Dibujar un pixel
        mov ah,0ch
        mov al,4  ; 4 is red with default palette

        mov cx, con
        top:
            mov con,cx ; Guardar valor de cx

            mov cx, posx  ; x = 50
            mov dx, posy ; y = 100
            int 10h    ; call BIOS Service
            
            add posx,1 ; Añado 1 a la posision en x
            mov cx, con ; Reestablecer valor de cx
        loop top


        ;mov al,03h  ; Modo de texto
        ;mov ah,0   ; function number 0
        ;int 10h    ; call BIOS Screen Service

        mov ax,4c00h
        int 21h



    end main
