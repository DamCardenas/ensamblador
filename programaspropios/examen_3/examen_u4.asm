;programa prog51.asm --- 
;Programa que dibuja una banderita y al presionar cualquier tecla 
;se desplaza hacia la derecha...

Pintar MACRO color, eIni, eFin
		mov ah, 06h			;Función para "limpiar" la pantalla
		mov al, 00h			;Líneas a desplazar
		mov bh, color		;Color de la fuente y el fondo
		mov cx, eIni		;Esq Sup Izq del área a "limpiar"
		mov dx, eFin		;Esq Inf Der del área a "limpiar"
		int 10h
ENDM

Columna MACRO  x,y,altura,clr
		mov ch,y
        mov cl,x 
        mov dh,y
		sub dh,01h
        add dh,altura
        mov dl,x
		Pintar clr, cx, dx
ENDM

Imprimir MACRO Mensaje
	mov ah, 09h
	mov dx, offset Mensaje
	int 21h
ENDM
		
Terminar MACRO 
	mov ax, 4c00h
	int 21h
ENDM

.MODEL SMALL
.STACK 20h
.DATA
	posx	 db 0ah
	posy	 db 0ah
	ncols    dw 06h
	color 	 dw	27h
    con 	 dw 00h
.CODE
	inicio: 
		mov ax, @Data
		mov ds, ax	
        Pintar 07h, 0000h, 184fh	;limpiar pantalla
        mov cx,ncols
        e_ciclo:
            mov con,cx
            Columna posx,posy,con,color
            add posx,01h
			add color,10h
            mov cx,con
        loop e_ciclo
		Terminar
		END inicio