;- Leer 2 numeros de una cifra
;- preguntar al usuario si desea hacer restas
;- operar
;- mostrar resultado

readkey macro
    ; Simulacion de readkey()
    mov ah,08h
    int 21h
endm

; Macro de finalizacion de aplicacion

fin macro
; Terminacion del programa
    mov ax,4c00h
    int 21h
endm 

imprimir macro txt
    mov dx,offset txt
    mov ah,09h
    int 21h
endm


imprimircaracter macro caracter
        mov dl,caracter
        mov ah,2h
        int 21h
endm 


nuevaLinea macro 
    mov dx,offset t_nuevo_renglon
    mov ah,09h
    int 21h
endm

esnumero macro var,outp
    mov al,var
    cmp al,30h
    ja esnumero_segundofiltro
    jb esnumero_no
    je esnumero_segundofiltro
    esnumero_segundofiltro:
        cmp al,39h
        ja esnumero_no
        jb esnumero_si
        je esnumero_si
    esnumero_no:
        mov outp,0000h
        jmp esnumero_final
    esnumero_si:
        mov outp,0001h
        jmp esnumero_final

    esnumero_final: 
endm


leercaracter macro output
    ; Lectura de caracter
    mov ah,1h ; Lee un caracter (ese caracter sera guardado en el registro al)
    int 21h ; Aplica interrupcion
    mov output,al ; Guardo el caracter en un buffer temporal
endm

leerNumero macro var
    imprimir t_leer_numero
    leercaracter var
    nuevaLinea
endm

leerOperacion macro 
    imprimir t_leer_operacion 
    leercaracter v_operador
    nuevaLinea
endm

sumar macro
mov al,v_numero_a
        mov bl,v_numero_b
        sub al,30h
        sub bl,30h
        add bh,al
        add bh,bl
        add bh,30h
        mov v_resultado,bh
endm

restar macro
        mov al,v_numero_a
        mov bl,v_numero_b
        sub al,bl
        add al,30h
        mov v_resultado,al
endm

resultado macro
    nuevaLinea
    imprimircaracter v_resultado
    nuevaLinea
    fin
endm




.model SMALL ; Segmento donde se selecciona el modelo de tipo pequeño o tiny 
.stack 20h
.data  ; Segmento de declaracion de variables
    t_nuevo_renglon db 10,13,'$'
    t_leer_numero db 'Ingrese un numero ->$'
    t_leer_operacion db 'Ingrese la operacion (0->+,1->-):$'
    t_error_nn db 'La entrada no es de tipo numerica... intente de nuevo...$'
    t_flecha db ' ->$'
    t_autor db 'Damian Cardenas Quiñones  15130686$'
    t_descripcion db 'Este programa Realiza operaciones de suma o resta sobre 2 numeros de 1 caracter.$'
    v_temp db 0
    v_resultado db 0
    v_operador db 0
    v_numero_a db 0
    v_numero_b db 0
.code ; Segmento de codigo ejecutable
    main :
        mov ax,@data
        mov ds,ax
        leerNumero v_numero_a
        leerNumero v_numero_b
        leerOperacion
        mov al,v_operador
        cmp al, 30h
        je essuma
        ja esresta
        essuma:
            sumar
            jmp final
        esresta:
            restar
        final:
            resultado
    end main
