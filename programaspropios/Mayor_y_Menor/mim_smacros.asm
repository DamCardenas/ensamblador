; Leer un numero de un digito N
; Leer N numeros para comparar
; Obtener el mayor y el menor de dichos numeros

.model SMALL 
.stack 20h
.data  
    clectura_numero db 'Ingrese un numero->$'
    cnl db 10,13,'$'
    cmayor db 'El mayor es:$'
    cmenor db 'El menor es:$'
    vauxiliar_mayor db 00h
    vauxiliar_menor db 255
.code 
    inicio: 
    
    mov ax,@data
    mov ds,ax
    
    
    mov dx,offset cnl
    mov ah,09h
    int 21h 
    
    mov dx, offset clectura_numero
    mov ah,09h
    int 21h 
    
    mov ah,1h 
    int 21h 
    mov cl,al
    
    sub cl,30h
    
    e_ciclo:
        
        mov dx, offset cnl
        mov ah,09h
        int 21h
        
        
        mov dx,offset clectura_numero
        mov ah,09h
        int 21h
        
        
        mov ah,1h 
        int 21h 
     
    
        cmp al,vauxiliar_menor
        jb e_menor
        ja e_sig_cmp
        je e_sig_cmp
        e_menor:
            mov vauxiliar_menor,al
        
        e_sig_cmp:
        cmp al,vauxiliar_mayor
        ja e_mayor
        je e_cmp_fin
        jb e_cmp_fin
        e_mayor:
            mov vauxiliar_mayor,al
            
        e_cmp_fin:
    loop e_ciclo
    
  
    mov dx,offset cnl
    mov ah,09h
    int 21h
    
  
    mov dx,offset cmayor
    mov ah,09h
    int 21h
    
   
    mov dl, vauxiliar_mayor
    mov ah,2h
    int 21h
  
    mov dx,offset cnl
    mov ah,09h
    int 21h
    
    mov dx,offset cmenor
    mov ah,09h
    int 21h
    
   
    mov dl, vauxiliar_menor
    mov ah,2h
    int 21h
    
    mov ax,4c00h
    int 21h
      
    end inicio 