; Leer un numero de un digito N
; Leer N numeros para comparar
; Obtener el mayor y el menor de dichos numeros


imprimir macro txt
    mov dx,offset txt
    mov ah,09h
    int 21h
endm

leercaracter macro output
    mov ah,1h 
    int 21h 
    mov output,al
endm

imprimircaracter macro caracter
    mov dl,caracter
    mov ah,2h
    int 21h
endm 

.model SMALL ; Segmento donde se selecciona el modelo de tipo pequeño o tiny 
.stack 20h
.data  ; Segmento de declaracion de variables
    clectura_numero db 'Ingrese un numero->$'
    cnl db 10,13,'$'
    cmayor db 'El mayor es:$'
    cmenor db 'El menor es:$'
    vauxiliar_mayor db 00h
    vauxiliar_menor db 255
.code ; Segmento de codigo ejecutable
    inicio: 
    
    mov ax,@data
    mov ds,ax
    
    imprimir cnl
    imprimir clectura_numero
    leercaracter cl
    sub cl,30h
    
    e_ciclo:
        imprimir cnl
        imprimir clectura_numero
        leercaracter al
        cmp al,vauxiliar_menor
        jb e_menor
        ja e_sig_cmp
        je e_sig_cmp
        e_menor:
            mov vauxiliar_menor,al
        
        e_sig_cmp:
        cmp al,vauxiliar_mayor
        ja e_mayor
        je e_cmp_fin
        jb e_cmp_fin
        e_mayor:
            mov vauxiliar_mayor,al
            
        e_cmp_fin:
    loop e_ciclo
    
    imprimir cnl
    imprimir cmayor
    imprimircaracter vauxiliar_mayor
    imprimir cnl
    imprimir cmenor
    imprimircaracter vauxiliar_menor
    
    mov ax,4c00h
    int 21h
      
    end inicio 