; Programa en ensamblador con sintaxis MASM

.model SMALL ; Segmento donde se selecciona el modelo de tipo pequeño o tiny 
.stack 20h
.data  ; Segmento de declaracion de variables
    mLC db 10,13,'Ingresa un caracter-> $'
    mCL db 10,13,'El caracter ingresado es:$'
.code ; Segmento de codigo ejecutable
    inicio: ; Label o etiqueta de inicio
        ;Precarga de segmento .data
        mov ax,@data
        mov ds,ax

        ; Despliegue de Mensaje para indicar al usuario
        ; Que debe ingresar un caracter
        mov dx,offset mLC
        mov ah,09h
        int 21h

        ; Lectura de caracter
        mov ah,1h ; Lee un caracter (ese caracter sera guardado en el registro al)
        int 21h ; Aplica interrupcion
        mov bl,al ; Guardo el caracter en un buffer temporal
        
        ; Imprime mensaje que indica al usuario que
        ; caracter introdujo
        mov dx,offset mCL
        mov ah,09h
        int 21h

        ; Movimiento de resultado
        mov dl,bl ; Mueve el resultado de al a dl

        ; Imprime caracter
        mov ah,2h
        int 21h

        ; Simulacion de readkey()
        mov ah,08h
        int 21h
        
        ; Terminacion del programa
        mov ax,4c00h
        int 21h
    end inicio ; Fin de etiqueta de inicio