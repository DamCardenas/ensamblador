; Tareas

; Modificar el programa 29  para que determine cual es el mayor de 2 numeros de 2 cifras

.model SMALL
.stack 20h
.data
    mensaje_lectura db 10,13,'Ingrese un numero-> $'
    mensaje_mayor db 10,13,'El mayor es-> $'
    mensaje_iguales db 10,13,'Los numeros son iguales :($'
    nuevo_renglon db 10,13,'$'
    tmpa db 0
    tmpb db 0
    nau db 0
    nad db 0
    nbu db 0
    nbd db 0
.code
    main:
        mov ax,@data
        mov ds,ax
        
        mov dx,offset mensaje_lectura
        mov ah,09h
        int 21h

        ;Lectura de primer valor
        ;Decenas
        mov ah,1h
        int 21h
        mov nad,al

        sub al,30h ; conversion a valor numerico
        mov tmpa,al ; Guardado de valor numerico 
        mov ax,0000 ; Limpieza de regisro
        mov al,tmpa ; instancia de registro
        mov dl,10; ; asignacion de multiplicador
        mul dl ; Multiplicacion
        mov tmpa,al ; Guardado de resultado
    
        ; Unidades
        mov ah,1h
        int 21h
        mov nau,al

        sub al,30h ; conversion a valor numerico
        add al,tmpa
        mov tmpa,al


        mov dx,offset mensaje_lectura
        mov ah,09h
        int 21h
        ;Lectura de segundo valor
        ;Decenas
        mov ah,1h
        int 21h
        mov nbd,al

        sub al,30h ; conversion a valor numerico
        mov tmpb,al ; Guardado de valor numerico 
        mov ax,0000 ; Limpieza de regisro
        mov al,tmpb ; instancia de registro
        mov dl,10; ; asignacion de multiplicador
        mul dl ; Multiplicacion
        mov tmpb,al ; Guardado de resultado
    
        ; Unidades
        mov ah,1h
        int 21h
        mov nbu,al

        sub al,30h ; conversion a valor numerico
        add al,tmpb
        mov tmpb,al

        ; Comparacion
        mov al,tmpa
        cmp al,tmpb
        ja va
        jb vb
        je iguales
        iguales:
            mov dx,offset mensaje_iguales
            mov ah,09h
            int 21h
            jmp fin
        va:
            mov dx,offset mensaje_mayor
            mov ah,09h
            int 21h
            mov dl,nad
            mov ah,2h
            int 21h
            mov dl,nau
            mov ah,2h
            int 21h
            jmp fin
        vb:
            mov dx,offset mensaje_mayor
            mov ah,09h
            int 21h
            mov dl,nbd
            mov ah,2h
            int 21h
            mov dl,nbu
            mov ah,2h
            int 21h
            jmp fin
        fin:
            mov ax,4c00h
            int 21h
    end main
