; -  Leer 1 caracter
; -  Leer 1 numero
; -  Leer n letras en base al numero anterior
; -  obtener el numero de ocurrencias que hay de la letra y la secuencia de caracteres
; -  Idicar al inicio # de control y nombre del alumno y una descripcion de lo que hace el programa


readkey macro
    ; Simulacion de readkey()
    mov ah,08h
    int 21h
endm

; Macro de finalizacion de aplicacion

fin macro
; Terminacion del programa
    mov ax,4c00h
    int 21h
endm 

imprimir macro txt
    mov dx,offset txt
    mov ah,09h
    int 21h
endm

nuevaLinea macro 
    mov dx,offset nuevo_renglon
    mov ah,09h
    int 21h
endm

esnumero macro var,outp
    mov al,var
    cmp al,30h
    ja esnumero_segundofiltro
    jb esnumero_no
    je esnumero_segundofiltro
    esnumero_segundofiltro:
        cmp al,39h
        ja esnumero_no
        jb esnumero_si
        je esnumero_si
    esnumero_no:
        mov outp,0000h
        jmp esnumero_final
    esnumero_si:
        mov outp,0001h
        jmp esnumero_final

    esnumero_final: 
endm


esletra macro var,outp
    mov al,var
    cmp al,41h
    jb esletra_no
    ja esletra_segundofiltro
    je esletra_si
    esletra_segundofiltro:
        cmp al,5Ah
        ja esletra_tercerfiltro
        jb esletra_si
        je esletra_si
    esletra_tercerfiltro:
        cmp al,61h
        je esletra_si
        jb esletra_no
        ja esletra_cuartofiltro
    esletra_cuartofiltro:
        cmp al,7ah
        je esletra_si
        jb esletra_si
        ja esletra_no
    esletra_si:
        mov outp,0001h
        jmp esletra_fin
    esletra_no:
        mov outp,0000h
        jmp esletra_fin
    esletra_fin:

endm

; comparacion de numero y añadido de conteo al contador
analisis macro letra_a,letra_b
    mov al,letra_a
    mov ah,letra_b
    cmp al,ah
    je analisis_s
    jb analisis_fin
    ja analisis_fin
    analisis_s:
        mov ah,c_coincidencias
        add ah,0001h
        mov c_coincidencias,ah
        jmp analisis_fin
    analisis_fin:
endm

imprimircaracter macro caracter
        mov dl,caracter
        mov ah,2h
        int 21h
endm 

leercaracter macro output
    ; Lectura de caracter
    mov ah,1h ; Lee un caracter (ese caracter sera guardado en el registro al)
    int 21h ; Aplica interrupcion
    mov output,al ; Guardo el caracter en un buffer temporal
endm





.model SMALL ; Segmento donde se selecciona el modelo de tipo pequeño o tiny 
.stack 20h
.data  ; Segmento de declaracion de variables
    t_nuevo_renglon db 10,13,'$'
    t_coincidencias db '# de coincidencias: $'
    t_leer_letra db 'Ingrese una letra ->$' 
    t_leer_cantidad db 'Ingrese Cantidad de letras ->$'
    t_leer_letra_b db 'Ingrese La letra #$'
    t_error_nn db 'La entrada no es de tipo numerica... intente de nuevo...$'
    t_flecha db ' ->$'
    t_autor db 'Damian Cardenas Quiñones  15130686$'
    t_descripcion db 'Este programa lee 1 caracter, lee un numero, lee N caracteres en base al numero anterior y devuelve un mensaje con el conteo de coincidencias.$'
    v_temp db 0
    v_letra db 0
    v_cantidad db 0
    c_coincidencias db 0
.code ; Segmento de codigo ejecutable
    inicio: ; Label o etiqueta de inicio
        ;Precarga de segmento .data
        mov ax,@data
        mov ds,ax
        imprimir t_autor
        imprimir t_nuevo_renglon
        imprimir t_descripcion
        imprimir t_nuevo_renglon
        inicio_leerLetra:
            imprimir t_leer_letra
            leercaracter v_letra
            imprimir t_nuevo_renglon
            mov bl,v_letra
            esletra bl,dx
            cmp dx,0001h
            jb inicio_leerLetra
            ja inicio_leerLetra
        inicio_leerCantidad:
            imprimir t_leer_cantidad
            leercaracter v_temp
            imprimir t_nuevo_renglon
            mov bl,v_temp
            esnumero bl,dx
            cmp dx,0001h
            je inicio_sn
            jb inicio_nn
            ja inicio_nn
            inicio_nn:
                imprimir t_nuevo_renglon
                imprimir t_error_nn
                imprimir t_nuevo_renglon
                jmp inicio_leerCantidad
            inicio_sn:
                mov bh,31h
                mov v_cantidad,bl
                inicio_lectura_char:
                    cmp bh,bl
                    jb inicio_lectura
                    je inicio_lectura
                    jmp inicio_ms
                    inicio_lectura:
                        imprimir t_nuevo_renglon 
                        imprimir t_leer_letra_b
                        imprimircaracter bh
                        imprimir t_flecha
                        leercaracter dl
                        analisis dl,v_letra
                        mov al,v_cantidad
                        add bh,01h
                        cmp bh,al
                        jb inicio_lectura_char
                        je inicio_lectura_char
                        ja inicio_ms

                inicio_ms:
                    ; Impresion de resultados
                    imprimir t_nuevo_renglon
                    imprimir t_coincidencias
                    mov al,c_coincidencias
                    add al,30h
                    imprimircaracter al
                    imprimir t_nuevo_renglon
        inicio_fin:
            readkey
            fin
    end inicio ; Fin de etiqueta de inicio