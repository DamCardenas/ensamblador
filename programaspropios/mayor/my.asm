; Programa que lee dos numeros de una cifra y que de de resultado 
; cual de los dos es mayor
.model SMALL
.stack 20h
.data
    mensaje_lectura db 10,13,'Ingrese un numero-> $'
    mensaje_mayor db 10,13,'El mayor es-> $'
    mensaje_iguales db 10,13,'Los numeros son iguales :($'
    nuevo_renglon db 10,13,'$'
    na db 0h
    nb db 0h
.code
    main:
        mov ax,@data
        mov ds,ax
        
        mov dx,offset mensaje_lectura
        mov ah,09h
        int 21h

        mov ah,1h
        int 21h
        mov na,al

        mov dx,offset mensaje_lectura
        mov ah,09h
        int 21h

        mov ah,1h
        int 21h
        mov nb,al

        mov dx,offset nuevo_renglon
        mov ah,09h
        int 21h

        mov al,nb
        cmp al,na
        ja va
        jb vb
        je iguales
        iguales:
            mov dx,offset mensaje_iguales
            mov ah,09h
            int 21h
            jmp fin
        va:
            mov dx,offset mensaje_mayor
            mov ah,09h
            int 21h
            mov dl,nb
            mov ah,2h
            int 21h
            jmp fin
        vb:
            mov dx,offset mensaje_mayor
            mov ah,09h
            int 21h
            mov dl,na
            mov ah,2h
            int 21h
            jmp fin 
        fin:
            mov ax,4c00h
            int 21h
    end main
