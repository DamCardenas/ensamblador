; Programa en ensamblador con sintaxis MASM

; Macro para imprimir mensaje
imprimir macro txt
    mov dx,offset txt
    mov ah,09h
    int 21h
endm

; Macro para imprimir caracter
imprimircaracter macro caracter
        mov dl,caracter
        mov ah,2h
        int 21h
endm 

;Macro para leer un caracter
leercaracter macro output
    ; Lectura de caracter
    mov ah,1h ; Lee un caracter (ese caracter sera guardado en el registro al)
    int 21h ; Aplica interrupcion
    mov output,al ; Guardo el caracter en un buffer temporal
endm

; Macro de simulacion de readkey
readkey macro
    ; Simulacion de readkey()
    mov ah,08h
    int 21h
endm

; Macro de finalizacion de aplicacion

fin macro
; Terminacion del programa
    mov ax,4c00h
    int 21h
endm 



.model SMALL ; Segmento donde se selecciona el modelo de tipo pequeño o tiny 
.stack 20h
.data  ; Segmento de declaracion de variables
    mLC db 10,13,'Ingresa un caracter-> $'
    mCL db 10,13,'El caracter ingresado es:$'
    car db 0

.code ; Segmento de codigo ejecutable
    inicio: ; Label o etiqueta de inicio
        ;Precarga de segmento .data
        mov ax,@data
        mov ds,ax

        ; Despliegue de Mensaje para indicar al usuario
        ; Que debe ingresar un caracter
        imprimir mLC

        leercaracter car
        
        ; Imprime mensaje que indica al usuario que
        ; caracter introdujo
        imprimir mCL

        imprimircaracter car
        readkey
        fin 
        
        
        
    end inicio ; Fin de etiqueta de inicio