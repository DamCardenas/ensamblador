;programa prog51.asm --- 
;Programa que dibuja una banderita y al presionar cualquier tecla 
;se desplaza hacia la derecha...

Pintar MACRO color, eIni, eFin
		mov ah, 06h			;Función para "limpiar" la pantalla
		mov al, 00h			;Líneas a desplazar
		mov bh, color		;Color de la fuente y el fondo
		mov cx, eIni		;Esq Sup Izq del área a "limpiar"
		mov dx, eFin		;Esq Inf Der del área a "limpiar"
		int 10h
ENDM

Jugador MACRO	x,y
		mov ch,y
        mov cl,x 
        mov dx,cx
		Pintar 27h, cx, dx
ENDM


Imprimir MACRO Mensaje
	mov ah, 09h
	mov dx, offset Mensaje
	int 21h
ENDM

ReadKey MACRO
	mov ah, 07h
	int 21h
ENDM
		
Terminar MACRO 
	mov ax, 4c00h
	int 21h
ENDM


.MODEL SMALL
.STACK 20h
.DATA
	cDescrip db 'Programa que dibuja una banderita y luego la desplaza hacia la derecha',10,13,'$'
	cFinProg db 10,13,'Fin del programa$'
	posx	 db 00h
	posy	 db 00h
	
.CODE
	inicio: 
		mov ax, @Data
		mov ds, ax	


		eDesplaza:
			Pintar 07h, 0000h, 184fh	;limpiar pantalla
			Jugador posx,posy
			ReadKey
			cmp al, 'a'
			je  e_izquierda
            cmp al, 's'
			je  e_abajo
            cmp al, 'w'
			je  e_arriba
            cmp al, 'd'
			je  e_derecha
            cmp al, 'q'
			je  e_fin
		e_derecha:
            add posx,01h
            jmp eDesplaza
        e_izquierda:
            sub posx,01h
            jmp eDesplaza
        e_arriba:
            sub posy,01h
            jmp eDesplaza
        e_abajo:
            add posy,01h
            jmp eDesplaza
        e_fin:
			Pintar 07h, 0000h, 184fh	;limpiar pantalla
		    Terminar
				
		END inicio