# Lenguajes de interfaz
### por Damian Cardenas Quiñones

Repositorio creado para fines didacticos y para reducir el consumo de memoria que significaba tener que hacer una maquina virtual para poder ejecutar programas en ensamblador...

## Contenido
- Ejecutables:
    - msedit
    - masm
    - link
    - debug
- Programas hechos en clase (Algunos x'D)
- Programas Propios
- Tabla de caracteres ascii
- Libro 'Assembly Lenguage for x86 Processors 6th ed.pdf'
- Mucho cariño 
 

## Usalo con Dosbox!

### Instrucciones de instalacion

- __1__. Descarga este [repositorio](https://bitbucket.org/DamCardenas/ensamblador/get/790ed3f7a18d.zip).
- __2__. Descomprimir el archivo zip descargado. 
- __3__. Descarga [Dosbox](https://www.dosbox.com/download.php?main=1).
```bash
# NOTA: Si usas algun sistema operativo basado en debian puedes instalarlo con solo escribir...
sudo apt-get install dosbox
```
- __4__. Abre y cierra Dosbox.
- __5__. Editar el archivo "dosbox-<version\>.conf".
```bash
#####################################################################
#                                                                   #
# Ubicaciones del archivo 'dosbox-<version>.conf'                   #
# Windows -> C:\Users\<nombre de usuario>\AppData\Local\DOSBox\     #
# Linux -> ~/.dosbox/                                               #
#                                                                   #
#####################################################################
#                                                                   #
# Nota: Es necesario abrir dosbox una vez despues de su instalacion #
# para que se genere el archivo 'dosbox-<version>.conf'             #
#                                                                   #
#####################################################################
# Agregar lo siguiente al final del archivo

mount c <direccion de descompresion del repositorio>
c:
PATH=c:\PROGRAMS
cls
```
- __6__. Ejecuta Dosbox

### Explicacion de la instalacion

Dentro de los pasos de instalacion  se realizan un par de cosas que pueden despertar algunas dudas, sobre todo en el paso __4__ y __5__.

__Paso 4__:

Se mensiona que hay que abrir y cerrar la aplicacion de dosbox, pero... Por que?

Despues de instalar la aplicacion de dosbox; el archivo "dosbox-<version\>.conf" no se genera en el momento de instalacion, si no, en el momento en el que nosotros ejecutamos dosbox por primera vez, por lo tanto, es necesario abrir y cerrar la aplicacion para generar el archivo que editaremos posteriormente.

__Paso 5__:

Este paso es completamente dependiente del paso 4, debido a que necesitamos que se se genere el archivo "dosbox-<version\>.conf" para su edicion.

Las lineas que se agregan al documento sirven para lo siguiente:

```bash
# Montaje del directorio del repositorio como una unidad c:
mount c <direccion de descompresion del repositorio>
```

```bash
# Con este comando nos transladamos a la raiz de la unidad c:
c:
```

```bash
# Establecemos la variable de entorno PATH
# Con esto hecho podremos ejecutar los ficheros '.exe' que se encuentren
# En el o los directorios especificados desde cualquier lugar
PATH=c:\PROGRAMS
```

```bash
# Limpiamos la pantalla para que el prompt quede limpio al iniciar dosbox
# No es tan necesario y se puede omitir
cls
```

### Instrucciones de uso 

- Ejecucion de DEBUG.exe
    
```bash
# Para iniciar debug
debug
# Para realizar debug sobre una aplicacion existente
debug tuapp.exe
```

- Ejecucion de MSEDIT.exe
    
```bash
# Para iniciar msedit
msedit
# Para cargar un archivo al iniciar msedit
msedit <direccion del archivo>
```

- Ejecucion de MASM.exe
    
```bash
# Generacion de archivo objeto
masm archivo.asm
```

- Ejecucion de LINK.exe

```bash
# Generacion de archivo .exe
link archivo.obj
```
    
- Edicion de archivo mediante editor de texto preferido (el mio es vscode)
![](Prueba.gif)

## Recursos extras
- [Tutorial sobre Ensamblador](https://www.youtube.com/watch?v=vtWKlgEi9js&list=PLPedo-T7QiNsIji329HyTzbKBuCAHwNFC) 
- [Tutorial sobre macros](https://www.youtube.com/watch?v=SSOqAJZ8omQ)
- [Tutorial de instalacion dosbox](https://www.youtube.com/watch?v=2nwZ0cO1FcU)

